from celery import Celery
from time import sleep


app = Celery('mikhal', broker='amqp://host.docker.internal:5672')


@app.task(name="mikhal")
def reverse(text):
    sleep(5)
    return text[::-1]
