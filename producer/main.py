from typing import List

from fastapi import FastAPI, status
from pydantic import BaseModel

from celery import Celery

celery_app = Celery('mikhal', broker='amqp://host.docker.internal:5672')


class Request(BaseModel):
    request_id: int
    customer_name: str
    model_id: int
    prediction_input: List[float]


app = FastAPI()


@app.post("/request/", status_code=status.HTTP_201_CREATED)
async def create_request(request: Request):
    celery_app.send_task('mikhal', [request.customer_name], queue='mikhal')
    return request
